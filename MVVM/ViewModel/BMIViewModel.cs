using UD5T2.MVVM.Model;

namespace UD5T2.MVVM.ViewModel;

public class BMIViewModel : ContentPage
{
    // Campo privado que representa el modelo BMI
    private BMI bmi;

    // Propiedad pública que expone el modelo BMI
    public BMI BMI
    {
        get; set; // Propiedad con getter y setter para acceder al modelo BMI
    }
    public BMIViewModel()
	{
        BMI = new BMI { Peso = 50, Altura = 25 }; //inicializar la propiedad
    }
}