using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UD5T2.MVVM.Model;

public class BMI : INotifyPropertyChanged
{
    // Propiedad privada para almacenar la altura.
    private float altura;

    // Propiedad pública Altura con notificación de cambios.
    public float Altura
    {
        get { return altura; }
        set
        {
            // Comprueba si el valor ha cambiado antes de actualizar.
            if (altura != value)
            {
                altura = value;

                // Notifica a los suscriptores que la propiedad Altura ha cambiado.
                OnPropertyChanged(nameof(Altura));
                // También notifica cambios en otras propiedades dependientes.
                OnPropertyChanged(nameof(Resultado));
                OnPropertyChanged(nameof(ResultadoBMI));
            }
        }
    }

    // Propiedad privada para almacenar el peso.
    private float peso;

    // Propiedad pública Peso con notificación de cambios.
    public float Peso
    {
        get { return peso; }
        set
        {
            // Comprueba si el valor ha cambiado antes de actualizar.
            if (peso != value)
            {
                peso = value;

                // Notifica a los suscriptores que la propiedad Peso ha cambiado.
                OnPropertyChanged(nameof(Peso));
                // También notifica cambios en otras propiedades dependientes.
                OnPropertyChanged(nameof(Resultado));
                OnPropertyChanged(nameof(ResultadoBMI));
            }
        }
    }

    // Propiedad calculada Resultado que depende de Altura y Peso.
    public float Resultado
    {
        get
        {
            // Fórmula para calcular el BMI.
            return Altura != 0 ? Peso / (Altura * Altura) * 10000 : 0;
        }
    }

    // Propiedad calculada ResultadoBMI que proporciona una descripción del BMI.
    public string ResultadoBMI
    {
        get
        {
            if (Resultado <= 16)
            {
                return "BMI: Delgado Severo";
            }
            else if (Resultado <= 17)
            {
                return "BMI: Delgado Moderado";
            }
            else if (Resultado <= 18.5)
            {
                return "BMI: Delgado Medio";
            }
            else if (Resultado <= 25)
            {
                return "BMI: Normal";
            }
            else if (Resultado <= 30)
            {
                return "BMI: Sobrepeso";
            }
            else if (Resultado <= 35)
            {
                return "BMI: Obesidad Clase I";
            }
            else if (Resultado <= 40)
            {
                return "BMI: Obesidad Clase II";
            }
            else
            {
                return "BMI: Obesidad Clase III";
            }
        }
    }

    // Evento que se dispara cuando cambia una propiedad.
    public event PropertyChangedEventHandler PropertyChanged;

    // Método protegido para invocar el evento PropertyChanged y notificar cambios en propiedades.
    protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}