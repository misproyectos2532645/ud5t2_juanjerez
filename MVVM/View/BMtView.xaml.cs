using UD5T2.MVVM.ViewModel;

namespace UD5T2.MVVM.View;

// Clase parcial BMtView que hereda de ContentPage.
public partial class BMtView : ContentPage
{
    // Constructor de la clase BMtView.
    public BMtView()
    {
        // Inicializa la página.
        InitializeComponent();

        // Establece el contexto de enlace (BindingContext) como una nueva instancia de BMIViewModel.
        BindingContext = new BMIViewModel();
    }
}