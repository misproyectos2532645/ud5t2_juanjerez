﻿using UD5T2.MVVM.View;

namespace UD5T2
{
    public partial class App : Application
    {
        //metodo que inicializa la App
        public App()
        {
            InitializeComponent();

            //página de inicio de la aplicación
            MainPage = new BMtView();
        }
    }
}
